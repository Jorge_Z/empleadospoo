/*
 * 
 */
package empleadospoo;

/**
 *
 * @author jz600
 */
public class Empleado {
    private String cedula, nombre, puesto;
    
    //Constructores después de los atributos
    public Empleado() {
    }

    public Empleado(String cedula, String nombre, String puesto) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.puesto = puesto;
    }
    
    //Setters & Getters
    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    @Override
    public String toString() {
        return "Empleado{" + "cedula=" + cedula + ", nombre=" + nombre + ", puesto=" + puesto + '}';
    }
    
    
}
