/*
 * 
 */
package empleadospoo;
import java.io.*;
/**
 *
 * @author jz600
 */
public class EmpleadosPOO {

    static BufferedReader in = new BufferedReader (new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Empleado[] empleados = new Empleado[10];
    public static void main(String[] args) throws IOException {
        menu();
    }
    
    public static void menu() throws IOException{
        int opcion;
        do {
            out.println("--------------- Registro de Empleados ---------------");
            out.println("-----------------------------------------------------");
            out.println("Seleccione una de las siguientes opciones:");
            out.println("1. Registrar un nuevo empleado.");
            out.println("2. Mostrar empleados.");
            out.println("3. Salir");
            out.println("Ingrese su opción:");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        }while (opcion != 3);
    }
    
    public static void procesarOpcion(int popcion) throws IOException{
        switch(popcion){
            case 1:
                registrarEmpleado();
                break;
            case 2:
                mostrarEmpleados();
                break;
            case 3:
                out.println("Gracias por usar el sistema.");
                break;
            default:
                out.println("Opción incorrecta, por favor verifique su selección.");
        }
    }
    
    public static void registrarEmpleado() throws IOException{
        String cedula, nombre, puesto;
        int i;
        out.println("***Registro de nuevo empleado***");
        out.print("Ingrese el número de cédula: ");
        cedula = in.readLine();
        out.print("Ingrese el nombre: ");
        nombre = in.readLine();
        out.print("Ingrese el puesto: ");
        puesto = in.readLine();
        out.println();
        Empleado empleado = new Empleado(cedula, nombre, puesto);
        for(i = 0; i < empleados.length; i++){
            if (empleados[i]== null){
                empleados[i] = empleado;
                /**Para evitar que el for llene el arreglo, se puede usar break o asignarle al contador
                el valor del tamaño del arreglo**/
                i = empleados.length;
            }
        }
        out.println("*Empleado registrado*");
        out.println(); 
    }
    
    public static void mostrarEmpleados(){
        int i;
        out.println("Empleados registrados: ");
        out.println();
        for (i = 0; i < empleados.length; i++){
            if(empleados[i] != null){
                out.println(empleados[i]);
            }
        }
    }
}
